import {
    GET_ALL_PRODUCTS_FAIL,
    GET_ALL_PRODUCTS_SUCCESS,
    LOADING_PRODUCTS
} from '../actions/types';

import { API_URL } from '../../helpers/apiurl';
import Axios from 'axios';

export const getAllProducts = () => {
    return async (dispatch) => {
        try {
            dispatch({ type: LOADING_PRODUCTS })
            console.log('a')

            var res = await Axios.get(API_URL + '/product')
            console.log(res.data, 'Get all product res')

            dispatch({
                type: GET_ALL_PRODUCTS_SUCCESS,
                payload: res.data
            })
        } catch (err) {
            console.log(err)
            dispatch({
                type: GET_ALL_PRODUCTS_FAIL,
                payload: err.response ? err.response.data.message : err.name
            })
        }
    }
}