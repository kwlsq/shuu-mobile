//action types LoginFormReducer.js
export const INPUT_USERNAME = 'INPUT_USERNAME';
export const LOADING_LOGIN = 'LOADING_LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';

//action types ProductReducer.js
export const GET_ALL_PRODUCTS_SUCCESS = 'GET_ALL_PRODUCTS_SUCCESS';
export const GET_ALL_PRODUCTS_FAIL = 'GET_ALL_PRODUCTS_FAIL';
export const LOADING_PRODUCTS = 'LOADING_PRODUCTS';
export const GET_STOCK_SUCCESS = 'GET_STOCK_SUCCESS';

//action types CartReducer.js
export const GET_PRODUCT_SIZE_SUCCESS = 'GET_PRODUCT_SIZE_SUCCESS';
export const GET_PRODUCT_SIZE_FAIL = 'GET_PRODUCT_SIZE_FAIL';
export const SELECT_SIZE = 'SELECT_SIZE';
export const ADD_TO_CART_SUCCESS = 'ADD_TO_CART_SUCCESS';
export const CLOSE_POPUP = 'CLOSE_POPUP';
export const GET_CART_DETAILS = 'GET_CART_DETAILS';
export const GET_TOTAL_PRICE = 'GET_TOTAL_PRICE';
export const LOADING_CART = 'LOADING_CART';
export const SET_QTY = 'SET_QTY';

//action types TransacitonReducer.js
export const CHECKOUT = 'CHECKOUT';
export const CLOSE_POPUP_CHECKOUT = 'CLOSE_POPUP_CHECKOUT';


