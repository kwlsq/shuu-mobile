import {
    GET_PRODUCT_SIZE_SUCCESS,
    GET_PRODUCT_SIZE_FAIL,
    SELECT_SIZE,
    ADD_TO_CART_SUCCESS,
    GET_STOCK_SUCCESS,
    CLOSE_POPUP,
    GET_TOTAL_PRICE,
    GET_CART_DETAILS,
    LOADING_CART,
    SET_QTY
} from './types';
import { API_URL } from '../../helpers/apiurl';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

export const getAllSize = (productNameId) => {
    return async (dispatch) => {
        try {
            console.log('check', productNameId)
            var res = await Axios.post(API_URL + '/product/size', { productNameId })
            console.log(res.data, 'Get all sizes')

            dispatch({
                type: GET_PRODUCT_SIZE_SUCCESS,
                payload: res.data
            })
        } catch (err) {
            console.log(err)
            dispatch({
                type: GET_PRODUCT_SIZE_FAIL,
                payload: err.response ? err.response.data.message : err.name
            })
        }
    }
}

export const setQty = (qty) => {
    return ({
        type: SET_QTY,
        payload: qty
    })
}

export const selectSize = (size, sizeId, productNameId) => {
    return async (dispatch) => {
        try {
            dispatch({
                type: SELECT_SIZE,
                payload: { size, sizeId }
            })

            var res = await Axios.post(API_URL + '/product/stock', { sizeId, productNameId })
            console.log(res.data, 'Get stock by sizes')

            dispatch({
                type: GET_STOCK_SUCCESS,
                payload: res.data.stock
            })
        } catch (err) {
            console.log(err)
        }
    }

}

export const addToCart = (productId, sizeId, quantity, stock) => {
    return async (dispatch) => {
        try {
            // await AsyncStorage.removeItem('username')
            const id = await AsyncStorage.getItem('id')
            console.log(id, productId, sizeId, quantity, stock, 'REQUIRED PARAMS FOR CART')
            var res = await Axios.post(API_URL + '/transaction/addtocart', { id, productId, sizeId, quantity, stock })
            console.log(res.data, 'current cart')

            dispatch({
                type: ADD_TO_CART_SUCCESS,
                payload: res.data
            })

        } catch (err) {
            console.log(err)
        }
    }
}

export const closePopup = () => {
    return ({ type: CLOSE_POPUP })
}

export const getCartDetail = () => {
    return async (dispatch) => {
        try {
            dispatch({ type: LOADING_CART })
            const id = await AsyncStorage.getItem('id')
            var res = await Axios.post(API_URL + '/transaction/cart', { id })
            console.log(res.data, 'cart detail')
            dispatch({
                type: GET_CART_DETAILS,
                payload: res.data
            })
        } catch (err) {
            console.log(err)
        }
    }
}

export const getTotalPrice = () => {
    return async (dispatch) => {
        try {
            dispatch({ type: LOADING_CART })
            const id = await AsyncStorage.getItem('id')
            var res = await Axios.post(API_URL + '/transaction/totalprice', { id })
            console.log(res.data, 'total price')
            dispatch({
                type: GET_TOTAL_PRICE,
                payload: res.data
            })
        } catch (err) {
            console.log(err)
        }
    }
}