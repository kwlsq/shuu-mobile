import {
    CHECKOUT, CLOSE_POPUP_CHECKOUT
} from '../actions/types';

import { API_URL } from '../../helpers/apiurl';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';


export const checkOut = (totalPrice) => {
    return async (dispatch) => {
        try {
            const id = await AsyncStorage.getItem('id')
            var res = await Axios.post(API_URL + '/transaction/checkout', { id, totalPrice })
            console.log(res.data, 'Success checkout')

            dispatch({
                type: CHECKOUT,
                payload: res.data
            })
        } catch (err) {
            console.log(err)
        }
    }
}

export const closePopUpCheckOut = () => {
    return ({ type: CLOSE_POPUP_CHECKOUT })
}