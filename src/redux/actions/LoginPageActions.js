import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {
    INPUT_USERNAME,
    LOADING_LOGIN,
    LOGIN_SUCCESS,
    LOGIN_FAIL
} from './types';
import { API_URL } from '../../helpers/apiurl';

export const onInputUsername = (string) => {
    console.log(string, ' = PAYLOAD ')
    return {
        type: INPUT_USERNAME,
        payload: string
    }
}


export const onUserLogin = (username) => {
    return async (dispatch) => {
        try {
            console.log('action', username)
            dispatch({ type: LOADING_LOGIN })

            var res = await axios.post(API_URL + '/user/login', { username })
            console.log(res.data["username"], 'username get')
            console.log(res.data["id"], 'id get')

            await AsyncStorage.setItem('username', res.data["username"])
            await AsyncStorage.setItem('id', `${res.data["id"]}`)
            console.log(await AsyncStorage.getItem('username'), 'async')
            console.log(await AsyncStorage.getItem('id'), 'async')

            dispatch({
                type: LOGIN_SUCCESS,
                payload: res.data["username"]
            })
        } catch (err) {
            console.log(err)
            dispatch({
                type: LOGIN_FAIL,
                payload: err.response ? err.response.data.message : err.name
            })
        }
    }
}

export const loginCheck = () => {
    return async (dispatch) => {
        try {
            var storage = await AsyncStorage.getItem('username')
            if (!storage) {
                return dispatch({ type: LOGIN_FAIL })
            }
            console.log(storage, 'storage')

            dispatch({
                type: LOGIN_SUCCESS,
                payload: storage
            })
        } catch (err) {
            console.log(err)
            dispatch({
                type: LOGIN_FAIL,
                payload: err.response ? err.response.data.message : err.name
            })
        }
    }
}

export const logOut = () => {
    return async () => {
        try {
            const user = await AsyncStorage.removeItem('username')
            const id = await AsyncStorage.removeItem('id')
            console.log(id, user, 'credential deleted')
        } catch (err) {
            console.log(err, 'failed to logout')
        }
    }
}