import { combineReducers } from 'redux';
import LoginFormReducer from './LoginFormReducer';
import UserReducer from './UserReducer';
import ProductReducer from './ProductReducer';
import CartReducer from './CartReducer';

export default combineReducers({
    loginForm: LoginFormReducer,
    user: UserReducer,
    product: ProductReducer,
    cart: CartReducer,
})