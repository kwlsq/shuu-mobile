import {
    INPUT_USERNAME,
    LOADING_LOGIN,
    LOGIN_FAIL,
    LOGIN_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
    username: '',
    error: '',
    loading: false
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case INPUT_USERNAME:
            return { ...state, username: action.payload }
        case LOADING_LOGIN:
            return { ...state, loading: true }
        case LOGIN_FAIL:
            return { ...state, loading: false, error: action.payload }
        case LOGIN_SUCCESS:
            return { INITIAL_STATE }
        default:
            return state;
    }
}