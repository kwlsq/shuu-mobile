import {
    GET_PRODUCT_SIZE_SUCCESS,
    GET_PRODUCT_SIZE_FAIL,
    SELECT_SIZE,
    ADD_TO_CART_SUCCESS,
    CLOSE_POPUP,
    GET_TOTAL_PRICE,
    GET_CART_DETAILS,
    LOADING_CART,
    CHECKOUT,
    CLOSE_POPUP_CHECKOUT,
    SET_QTY
} from '../actions/types';

const INITIAL_STATE = {
    sizeLists: null,
    size: null,
    qty: 1,
    sizeId: null,
    cartLists: null,
    popUp: false,
    cartDetails: null,
    totalPrice: 0,
    loading: false,
    popUpCheckOut: false
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_PRODUCT_SIZE_SUCCESS:
            return { ...state, sizeLists: action.payload }
        case GET_PRODUCT_SIZE_FAIL:
            return { ...state, error: action.payload }
        case SELECT_SIZE:
            return { ...state, size: action.payload.size, sizeId: action.payload.sizeId }
        case SET_QTY:
            return { ...state, qty: action.payload }
        case ADD_TO_CART_SUCCESS:
            return { ...state, cartLists: action.payload, popUp: true, sizeId: null, size: null, qty: 1 }
        case CLOSE_POPUP:
            return { ...state, popUp: false }
        case GET_CART_DETAILS:
            return { ...state, cartDetails: action.payload, loading: false }
        case GET_TOTAL_PRICE:
            return { ...state, totalPrice: action.payload, loading: false }
        case LOADING_CART:
            return { ...state, loading: true }
        case CHECKOUT:
            return { ...state, popUpCheckOut: true }
        case CLOSE_POPUP_CHECKOUT:
            return { ...state, popUpCheckOut: false }
        default:
            return state;
    }
}
