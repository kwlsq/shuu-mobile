import {
    GET_ALL_PRODUCTS_SUCCESS,
    GET_ALL_PRODUCTS_FAIL,
    LOADING_PRODUCTS,
    GET_STOCK_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
    productLists: null,
    productStock: null,
    loading: false,
    error: ''
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_ALL_PRODUCTS_SUCCESS:
            console.log(action.payload, 'reducer product')
            return { ...state, productLists: action.payload, loading: false }
        case GET_ALL_PRODUCTS_FAIL:
            return { ...state, loading: false, error: action.payload }
        case GET_STOCK_SUCCESS:
            console.log(action.payload, 'STOCK BY SIZE')
            return { ...state, productStock: action.payload }
        case LOADING_PRODUCTS:
            return { ...state, loading: true }
        default:
            return state;
    }
}