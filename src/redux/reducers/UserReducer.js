import {
    LOGIN_SUCCESS,
    LOGIN_FAIL
} from '../actions/types';

const INITIAL_STATE = {
    authChecked: false,
    username: ''
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return { username: action.payload, authChecked: true }
        case LOGIN_FAIL:
            return { authChecked: true }
        default:
            return state
    }
}