import React, { useState } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { selectSize } from '../redux/actions';

export default ({ productNameId }) => {
    const dispatch = useDispatch();

    const size = useSelector((state) => state.cart.sizeLists)
    const chosenSize = useSelector((state) => state.cart.size)

    const {
        mainViewStyle,
        buttonSizePressedStyle,
        buttonSizetyle
    } = styles;

    const renderSizeButtons = () => {
        return size.map((val, index) => {
            return (
                <TouchableWithoutFeedback key={index}>
                    {
                        chosenSize === val.size
                            ?
                            <Text
                                key={index}
                                style={buttonSizePressedStyle}
                                onPress={() => dispatch(selectSize(val.size, val.id, productNameId))}
                            >
                                {val.size}
                            </Text>
                            :
                            <Text
                                key={index}
                                style={buttonSizetyle}
                                onPress={() => dispatch(selectSize(val.size, val.id, productNameId))}
                            >
                                {val.size}
                            </Text>
                    }
                </TouchableWithoutFeedback>
            )
        })
    }

    return (
        <View style={mainViewStyle}>
            <Text style={{ fontSize: 32 }}>Choose Size : </Text>
            {renderSizeButtons()}
        </View >
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: 'center'
    }, buttonSizePressedStyle: {
        padding: 10,
        fontSize: 24,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: '#b4faba'
    }, buttonSizetyle: {
        padding: 10,
        fontSize: 24,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: 'white'
    }

})