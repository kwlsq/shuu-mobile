import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { API_URL } from '../helpers/apiurl';


export default ({ cart, navigation }) => {
    const {
        mainViewStyle,
        imageViewStyle,
        detailViewStyle,
        priceViewStyle
    } = styles;
    return (
        <View style={mainViewStyle}>
            <View style={imageViewStyle}>
                <Image source={{ uri: `${API_URL}${cart.image}` }} style={{ flex: 1 }} />
            </View>
            <View style={detailViewStyle}>
                <Text style={{ fontSize: 18, textAlign: 'center', marginBottom: 5 }}>{cart.product_name}</Text>
                <Text style={{ fontSize: 16 }}>Brand : {cart.brand}</Text>
                <Text style={{ fontSize: 16 }}>Size : {cart.size}</Text>
            </View>
            <View style={priceViewStyle}>
                <Text>Quantity x Price per item  </Text>
                <Text>({cart.quantity} x {cart.price_per_item}) = </Text>
                <Text>{cart.price_times_quantity}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5,
        margin: 7.5,
        height: 200,
        flexDirection: 'row',
        justifyContent: "center"
    },
    imageViewStyle: {
        flex: 1,
        margin: 7.5,
    },
    detailViewStyle: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    priceViewStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 16
    }
})
