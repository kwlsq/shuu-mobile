import React, { useEffect } from 'react';
import { View, Text } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { logOut } from '../redux/actions'
import { useDispatch } from 'react-redux';

export default ({ navigation }) => {
    const dispatch = useDispatch()

    const onPressLogOut = () => {
        dispatch(logOut())
        navigation.navigate('Login')
    }

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableWithoutFeedback onPress={() => onPressLogOut()}>
                <Text style={{ backgroundColor: 'red', width: '100%' }}>Log out</Text>
            </TouchableWithoutFeedback>
        </View>
    )
}

