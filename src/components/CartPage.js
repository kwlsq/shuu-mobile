import React, { useEffect } from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import { Header } from 'react-native-elements';
import CartCard from './CartCard';
import { useSelector, useDispatch } from 'react-redux';
import { getCartDetail, getTotalPrice, checkOut } from '../redux/actions'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import CheckOutModal from './CheckOutModal';

export default ({ navigation }) => {
    const dispatch = useDispatch()
    const cartDetails = useSelector((state) => state.cart.cartDetails)
    const totalPrice = useSelector((state) => state.cart.totalPrice)
    const loadingCart = useSelector((state) => state.cart.loading)

    const onRefreshCart = () => {
        dispatch(getCartDetail())
        dispatch(getTotalPrice())
    }

    const {
        containerStyle,
        checkoutViewStyle
    } = styles
    return (
        <View style={containerStyle}>
            <Header
                leftComponent={{
                    icon: 'arrow-back',
                    tcolor: 'black',
                    onPress: () => navigation.goBack()
                }}
                centerComponent={{
                    text: 'Your Cart',
                    style: { color: 'black', fontSize: 18, fontWeight: '700' }
                }}
                containerStyle={{
                    backgroundColor: '#fff',
                    justifyContent: 'space-around',
                    elevation: 2,
                    marginTop: Platform.OS === 'ios' ? 0 : - 25,
                }}
            />
            <FlatList
                data={cartDetails}
                renderItem={({ item }) => <CartCard cart={item} totalPrice={totalPrice} navigation={navigation} />}
                keyExtractor={item => item.id.toString()}
                style={{ width: '100%', flex: 1 }}
                onRefresh={() => onRefreshCart()}
                refreshing={loadingCart}
            />
            <View style={checkoutViewStyle}>
                <CheckOutModal />
                <Text style={{ width: '100%', textAlign: "center", fontSize: 24, backgroundColor: "#80ff8a" }}>Total Price = {totalPrice[0].total_price}</Text>
                <View style={{ width: '100%', backgroundColor: "#68bed4" }}>
                    <TouchableWithoutFeedback onPress={() => dispatch(checkOut(totalPrice))}>
                        <Text style={{ fontSize: 24, textAlign: "center" }}>Checkout</Text>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor: '#fff',
        flex: 1,
        alignItems: 'center'
    },
    checkoutViewStyle: {
        justifyContent: "center",
        alignItems: 'center',
        width: "100%",
        padding: 0
    }
})
