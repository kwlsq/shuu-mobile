import React from 'react';
import { Image, TouchableWithoutFeedback } from 'react-native';
import { useDispatch } from 'react-redux';
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { getAllSize } from '../redux/actions'
import { API_URL } from '../helpers/apiurl';

export default ({ product, navigation }) => {
    const dispatch = useDispatch()
    const onPressProduct = async () => {
        await dispatch(getAllSize(product.productNameId))
        await navigation.navigate('ProductDetail', product)
    }

    return (
        <TouchableWithoutFeedback
            onPress={() => onPressProduct()}
        >
            <Card style={{ width: '96%', alignSelf: 'center' }}>
                <CardItem>
                    <Left>
                        <Body>
                            <Text>{product.product_name}</Text>
                            <Text note>{product.brand}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem cardBody>
                    <Image source={{ uri: `${API_URL}${product.image}` }} style={{ height: 350, flex: 1 }} />
                </CardItem>
                <CardItem>
                    <Left>
                        <Text>{product.price}</Text>
                    </Left>
                </CardItem>
            </Card>
        </TouchableWithoutFeedback>
    )
}