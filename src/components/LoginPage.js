import React from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import { Input, Button, Text } from 'react-native-elements';
import { StackActions } from '@react-navigation/native'
import {
    onInputUsername,
    onUserLogin,
    loginCheck
} from '../redux/actions';

class LoginPage extends React.Component {
    componentDidMount() {
        this.props.loginCheck();
    }

    componentDidUpdate() {
        if (this.props.user.username) {
            this.props.navigation.dispatch(StackActions.replace('TabNav'))
        }
    }

    onBtnLoginPress = () => {
        this.props.onUserLogin(this.props.loginForm.username)
    }

    render() {
        if (this.props.user.authChecked && !this.props.user.username) {
            return (
                <View style={styles.containerStyle}>
                    <Input
                        style={styles.inputContainerStyle}
                        placeholder="Username"
                        onChange={(val) => this.props.onInputUsername(val.nativeEvent.text)}
                        value={this.props.loginForm.username}
                    />
                    <Text style={{ color: 'red' }}>{this.props.loginForm.error}</Text>
                    <Button
                        title="Login"
                        containerStyle={{ width: '95%', margin: 10 }}
                        buttonStyle={{ backgroundColor: 'black' }}
                        onPress={this.onBtnLoginPress}
                        loading={this.props.loginForm.loading}
                    />

                </View>
            )
        }
        return (
            <View style={styles.containerStyle}>
                <Text h3 >Shuu</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    inputContainerStyle: {
        marginTop: 50,
        marginBottom: 100,
        width: '100%',
    }
})

const mapStateToProps = ({ loginForm, user }) => {
    return { loginForm, user }
}


export default connect(mapStateToProps, {
    onInputUsername,
    onUserLogin,
    loginCheck
})(LoginPage)