import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import { addToCart, setQty } from '../redux/actions'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import AddToCartModal from './AddToCartModal';

export default ({ stock, productId, navigation }) => {
    const dispatch = useDispatch()
    const qty = useSelector((state) => state.cart.qty)
    const [btnRemoveDisable, setBtnRemoveDisabled] = useState(false)
    const [btnAddDisable, setBtnAddDisabled] = useState(false)
    const sizeId = useSelector((state) => state.cart.sizeId)
    console.log(sizeId)

    useEffect(() => {
        if (qty <= 1) {
            setBtnRemoveDisabled(true);
            dispatch(setQty(1))
        } else if (qty >= stock) {
            setBtnAddDisabled(true);
            dispatch(setQty(stock))
        } else {
            setBtnAddDisabled(false);
            setBtnRemoveDisabled(false);
        }
    })
    console.log(qty)
    const {
        mainViewStyle,
        qtySelectorViewStyle,
        textInputStyle,
        buttonAddToCartStyle
    } = styles;

    return (
        <View style={mainViewStyle}>
            <View style={qtySelectorViewStyle}>
                <Button
                    icon={
                        <Icon
                            name="remove"
                            size={15}
                            color="white"
                        />
                    }
                    onPress={() => dispatch(setQty(qty - 1))}
                    disabled={btnRemoveDisable}
                />
                <TextInput
                    style={textInputStyle}
                    onChangeText={(e) => dispatch(setQty(parseInt(e)))}
                    value={JSON.stringify(qty)}
                    keyboardType='number-pad'
                    maxLength={stock}
                />
                <Button
                    icon={
                        <Icon
                            name="add"
                            size={15}
                            color="white"
                        />
                    }
                    onPress={() => dispatch(setQty(qty + 1))}
                    disabled={btnAddDisable}
                />
            </View>
            <TouchableWithoutFeedback
                style={buttonAddToCartStyle}
                onPress={() => dispatch(addToCart(productId, sizeId, qty, stock))}
            >
                <Text style={{ color: "white", fontSize: 24 }}>Add to cart</Text>
                <AddToCartModal navigation={navigation} />
            </TouchableWithoutFeedback>
        </View>
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    qtySelectorViewStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInputStyle: {
        borderWidth: 1,
        borderColor: 'black',
        minWidth: 100,
        marginHorizontal: 10
    },
    buttonAddToCartStyle: {
        height: 50,
        flexDirection: 'row',
        borderRadius: 5,
        backgroundColor: '#f01f5e',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10
    }

})