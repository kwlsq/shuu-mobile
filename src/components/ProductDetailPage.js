import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { Header } from 'react-native-elements';
import { useSelector } from 'react-redux';
import { API_URL } from '../helpers/apiurl';
import SizeComp from './SizeComp';
import AddtoCartComp from './AddToCartComp';

export default ({ route, navigation }) => {
    const product = route.params;
    const stock = useSelector((state) => state.product.productStock)

    const {
        mainViewStyle,
        imageViewStyle,
        detailProductViewStyle,
        sizeViewStyle,
        productInfoViewStyle,
        addToCartViewStyle,
        productInfoStyle
    } = styles;

    return (
        <View style={mainViewStyle}>
            <Header
                leftComponent={{
                    icon: 'arrow-back',
                    tcolor: 'black',
                    onPress: () => navigation.goBack()
                }}
                centerComponent={{
                    text: product.product_name,
                    style: { color: 'black', fontSize: 18, fontWeight: '700' }
                }}
                containerStyle={{
                    backgroundColor: '#fff',
                    justifyContent: 'space-around',
                    elevation: 2,
                    marginTop: Platform.OS === 'ios' ? 0 : - 25,
                }}
            />
            <View style={imageViewStyle}>
                <Image source={{ uri: `${API_URL}${product.image}` }} style={{ maxHeight: 500, flex: 1 }} />
            </View>
            <View style={detailProductViewStyle}>
                <View style={sizeViewStyle}>
                    <SizeComp productNameId={product.productNameId} />
                </View>
                <View style={productInfoViewStyle}>
                    <Text style={productInfoStyle}>Brand : {product.brand}</Text>
                    <Text style={productInfoStyle}>Price : {product.price}</Text>
                    {
                        stock === null
                            ?
                            <Text style={productInfoStyle}>Stock : {product.stock}</Text>
                            :
                            <Text style={productInfoStyle}>Stock : {stock}</Text>
                    }
                </View>
                <View style={addToCartViewStyle}>
                    {
                        stock === null
                            ?
                            <AddtoCartComp stock={product.stock} productId={product.id} navigation={navigation} />
                            :
                            <AddtoCartComp stock={stock} productId={product.id} navigation={navigation} />

                    }
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        alignItems: 'center'
    },
    imageViewStyle: {
        flex: 1,
        height: 300,
        width: "100%"
    },
    detailProductViewStyle: {
        flex: 1,
        width: "97.5%",
        borderRadius: 10,
        margin: 7.5,
        backgroundColor: '#fafafa'
    },
    sizeViewStyle: {
        flex: 1,
    },
    productInfoViewStyle: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
    },
    addToCartViewStyle: {
        flex: 1,
    },
    productInfoStyle: {
        fontSize: 24
    }
})