import React from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, FlatList } from 'react-native';
import { Header, Button } from 'react-native-elements';
import { getAllProducts, getTotalPrice, getCartDetail } from '../redux/actions'
import ProductCard from './ProductCard';


class HomePage extends React.Component {
    componentDidMount() {
        this.props.getAllProducts()
        this.props.getCartDetail()
        this.props.getTotalPrice()
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <Header
                    leftComponent={{
                        text: 'Shuu',
                        style: { color: 'black', fontSize: 18, fontWeight: '700' }
                    }}
                    leftContainerStyle={{ flex: 3 }}
                    containerStyle={{
                        backgroundColor: '#fff',
                        justifyContent: 'space-around',
                        marginTop: Platform.OS === 'ios' ? 0 : -25,
                        elevation: 2
                    }}
                    rightComponent={this.props.user.username}
                />
                <FlatList
                    data={this.props.product.productLists}
                    renderItem={({ item }) => <ProductCard product={item} navigation={this.props.navigation} />}
                    keyExtractor={item => item.id.toString()}
                    style={{ width: '100%' }}
                    onRefresh={() => this.props.getAllProducts()}
                    refreshing={this.props.product.loading}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor: '#fff',
        flex: 1,
        alignItems: 'center'
    }
})

const mapStateToProps = ({ user, product }) => {
    return { user, product }
}


export default connect(mapStateToProps, {
    getAllProducts,
    getCartDetail,
    getTotalPrice
})(HomePage)