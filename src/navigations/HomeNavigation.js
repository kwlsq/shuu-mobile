import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ProductDetail from '../components/ProductDetailPage';
import Home from '../components/HomePage'

const Stack = createStackNavigator()

export default (props) => {
    return (
        <Stack.Navigator
            initialRouteName="Home"
            headerMode="none"
        >
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="ProductDetail" component={ProductDetail} />
        </Stack.Navigator>
    )
}