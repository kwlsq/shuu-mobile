import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginPage from '../components/LoginPage';
import TabNav from './TabNavigation'

const Stack = createStackNavigator()

export default (props) => {
    return (
        <Stack.Navigator initialRouteName="Login" headerMode='none'>
            <Stack.Screen name="Login" component={LoginPage} />
            <Stack.Screen name="TabNav" component={TabNav} />
        </Stack.Navigator>
    )
}
