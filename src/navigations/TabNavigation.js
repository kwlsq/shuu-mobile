import React from 'react';
import { Icon } from 'react-native-elements'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeNav from '../navigations/HomeNavigation';
import Cart from '../components/CartPage';
import Account from '../components/AccountPage';

const Tab = createBottomTabNavigator();

export default (props) => {
    return (
        <Tab.Navigator
            initialRouteName="HomeNav"
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    if (route.name === 'HomeNav') {
                        iconName = 'ios-home'
                    }
                    else if (route.name === 'Cart') {
                        iconName = 'ios-cart';
                    } else if (route.name === 'Account') {
                        iconName = 'ios-person'
                    }

                    return <Icon name={iconName} size={35} color={color} type="ionicon" />;
                },
            })}
            tabBarOptions={{
                activeTintColor: 'black',
                inactiveTintColor: 'gray',
                showLabel: false
            }}
        >
            <Tab.Screen name="HomeNav" component={HomeNav} />
            <Tab.Screen name="Cart" component={Cart} />
            <Tab.Screen name="Account" component={Account} />
        </Tab.Navigator>
    )
}